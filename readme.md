# Project Title

Quiz project that focuses on testing the students and on CMS 


## Getting Started
Make sure you have installed:

*[Node](https://nodejs.org/en/) 
*[MySqlServer] (https://dev.mysql.com/downloads/mysql/) or XAMP set on port 3306

### Prerequisites

* Run all the queries from ```sql/queries/``` and ```sql/stored_procedure/``` consequently

### Installing

Using git

```
 git clone https://gitlab.com/cristian.cernat97/quiz-ceiti
```
or download the zip file
## Built With

* [Materialize](https://materializecss.com/) 
* [JQuery](https://jquery.com/) 
* [MySQL](https://www.mysql.com/) 
* [ChartJS](https://www.chartjs.org/) 
* [MomentJS](https://momentjs.com/) 
* [Handlebars](https://handlebarsjs.com/)
* [BCRYPT](https://www.npmjs.com/package/bcrypt)

## Contributing

Go ahead blame me for anything I am still learning 
## Versioning

Version 3.017

## Authors

* **Cristian Cernat** - (https://gitlab.com/cristian.cernat97)