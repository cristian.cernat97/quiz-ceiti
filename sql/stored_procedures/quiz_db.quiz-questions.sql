CREATE PROCEDURE get_quiz_questions(IN from_quiz INT)
  BEGIN
    SELECT @shuffle := quiz.random_shuffle
    FROM quiz_db.quiz
    WHERE quiz.quiz_id = from_quiz;

    -- creating temporary tables for easy insert
    CREATE TEMPORARY TABLE tmp_questions (
      id      INT,
      name    VARCHAR(300)
              CHARSET utf8 NOT NULL,
      type    VARCHAR(50)  NOT NULL,
      points  INT,
      answers VARCHAR(1000)
              CHARSET utf8 NOT NULL
    )
      ENGINE = MEMORY;

    CREATE TEMPORARY TABLE tmp_correct_answers (
      id              INT,
      correct_answers VARCHAR(1000)
                      CHARSET utf8 NOT NULL
    )
      ENGINE = MEMORY;

    CREATE TEMPORARY TABLE tmp_result (
      id              INT,
      name            VARCHAR(300)
                      CHARSET utf8 NOT NULL,
      type            VARCHAR(50)  NOT NULL,
      points          INT,
      answers         VARCHAR(1000)
                      CHARSET utf8 NOT NULL,
      correct_answers VARCHAR(1000)
                      CHARSET utf8 NOT NULL
    )
      ENGINE = MEMORY;

    -- insert question id, text and all answers (as text) into first tmp table
    INSERT INTO tmp_questions (id, name, type, points, answers)
      SELECT
        DISTINCT
        questions.question_id             AS id,
        questions.question_name           AS name,
        question_types.question_type_name AS type,
        questions.points                  AS points,
        GROUP_CONCAT(
            DISTINCT answers.answer_name
            -- selected separator is '~~'
            SEPARATOR '~~'
        )                                 AS answers
      FROM
        quiz_db.questions
        INNER JOIN quiz_db.question_answers ON questions.question_id = question_answers.question_id
        INNER JOIN quiz_db.answers ON question_answers.answer_id = answers.answer_id
        INNER JOIN quiz_db.question_types ON questions.question_type_id = question_types.question_type_id
        INNER JOIN quiz_db.quiz_questions ON quiz_questions.question_id = questions.question_id
        INNER JOIN quiz_db.quiz ON quiz_questions.quiz_id = quiz.quiz_id AND quiz.quiz_id = from_quiz
      GROUP BY
        (questions.question_id)
      ORDER BY
        CASE
        WHEN @shuffle = 1
          THEN RAND()
        WHEN @shuffle = 0
          THEN questions.question_id
        END DESC;

    -- insert correct answers into second tmp table
    INSERT INTO tmp_correct_answers (id, correct_answers)
      SELECT
        correct_answ_tb.id,
        correct_answ_tb.correct_answers
      FROM (
             SELECT
               question_answers.question_id AS id,
               GROUP_CONCAT(
                   DISTINCT answers.answer_name
                   -- selected separator is '~~'
                   SEPARATOR '~~'
               )                            AS correct_answers
             FROM
               quiz_db.answers
               INNER JOIN quiz_db.question_answers
                 ON answers.answer_id = question_answers.answer_id
                    AND question_answers.is_correct = 1
             GROUP BY
               question_answers.question_id
           ) AS correct_answ_tb
        INNER JOIN tmp_questions
          ON correct_answ_tb.id = tmp_questions.id;

    -- insert result sets of first and second temp tables into a third one for easy select
    INSERT INTO tmp_result (id, name, type, points, answers, correct_answers)
      SELECT
        tmp_questions.id,
        tmp_questions.name,
        tmp_questions.type,
        tmp_questions.points,
        tmp_questions.answers,
        tmp_correct_answers.correct_answers
      FROM
        tmp_questions
        INNER JOIN tmp_correct_answers ON tmp_questions.id = tmp_correct_answers.id;
    -- select from third tmp table
    SELECT *
    FROM
      tmp_result;

    DROP TEMPORARY TABLE tmp_questions;
    DROP TEMPORARY TABLE tmp_correct_answers;
    DROP TEMPORARY TABLE tmp_result;
  END;

CALL get_quiz_questions(1);
