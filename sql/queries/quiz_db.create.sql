CREATE DATABASE IF NOT EXISTS quiz_db;
USE quiz_db;
-- database tables
CREATE TABLE topics (
	topic_id INT AUTO_INCREMENT NOT NULL,
	topic_name VARCHAR (100) CHARSET utf8 NOT NULL,
    knowledge_of VARCHAR (100) CHARSET utf8 NOT NULL,
	PRIMARY KEY (topic_id),
	UNIQUE(topic_name)
);
CREATE TABLE answers (
	answer_id INT AUTO_INCREMENT NOT NULL,
	answer_name VARCHAR (300) CHARSET utf8 NOT NULL,
	PRIMARY KEY (answer_id)
);
CREATE TABLE question_types (
	question_type_id INT AUTO_INCREMENT NOT NULL,
	question_type_name VARCHAR (10) CHARSET utf8 NOT NULL,
	PRIMARY KEY (question_type_id),
	UNIQUE(question_type_name)
);
CREATE TABLE questions (
	question_id INT AUTO_INCREMENT NOT NULL,
	question_name VARCHAR (1000) CHARSET utf8 NOT NULL,
	question_type_id INT NOT NULL,
    points INT NOT NULL,
	PRIMARY KEY (question_id),
	FOREIGN KEY (question_type_id) REFERENCES question_types (question_type_id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE question_answers (
	question_id INT NOT NULL,
	answer_id INT NOT NULL,
	is_correct BIT NOT NULL,
	FOREIGN KEY (question_id) REFERENCES questions (question_id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (answer_id) REFERENCES answers (answer_id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE roles (
	role_id INT AUTO_INCREMENT NOT NULL,
	role_name VARCHAR (255) CHARSET utf8 NOT NULL,
	PRIMARY KEY (role_id),
	UNIQUE(role_name)
);
CREATE TABLE users (
	user_id INT AUTO_INCREMENT NOT NULL,
	user_name VARCHAR (255) CHARSET utf8 NOT NULL,
	user_password VARCHAR (255) NULL,
	role_id INT NOT NULL,
	PRIMARY KEY (user_id),
    FOREIGN KEY (role_id) REFERENCES roles (role_id) ON DELETE CASCADE ON UPDATE CASCADE,
	UNIQUE(user_name)
);
CREATE TABLE quiz (
	quiz_id INT AUTO_INCREMENT NOT NULL,
	quiz_name VARCHAR (255) CHARSET utf8 NOT NULL,
	quiz_password varchar(255) NOT NULL,
	topic_id INT NULL,
    random_shuffle BIT NOT NULL,
    time_limit INT NOT NULL,
    created_on TIMESTAMP,
	PRIMARY KEY (quiz_id),
    FOREIGN KEY (topic_id) REFERENCES topics (topic_id) ON DELETE CASCADE ON UPDATE CASCADE,
	UNIQUE(quiz_name)
);
CREATE TABLE quiz_questions (
	quiz_id INT NOT NULL,
	question_id INT NOT NULL,
  	FOREIGN KEY (quiz_id) REFERENCES quiz (quiz_id) ON DELETE CASCADE ON UPDATE CASCADE,
  	FOREIGN KEY (question_id) REFERENCES questions (question_id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE user_quiz_stats (
	user_id INT NOT NULL,
	quiz_id INT NOT NULL,
	mark INT NOT NULL,
	score INT NOT NULL,
    solved_correct INT NOT NULL,
    solved_incorrect INT NOT NULL,
	skipped INT NOT NULL,
	solved_on TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (quiz_id) REFERENCES quiz (quiz_id) ON DELETE CASCADE ON UPDATE CASCADE
);