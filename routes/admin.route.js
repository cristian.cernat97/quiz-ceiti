var express = require('express');
var router = express.Router();
var Q = require('q');
var connection = require('../configs/connection');
var bcrypt = require('bcryptjs');
var services = require('../configs/services');
var passport = require('passport');
var flash = require('connect-flash');
var LocalStrategy = require('passport-local').Strategy;
// check if user is logged in
function authorization(req, res, next) {
	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();
	// user: req.user ucide sesiunea userului
	// if they aren't redirect them to the home page
	res.redirect('/admin/sign-in');
}
router.post('/sign-form', passport.authenticate('local-login', {
	successRedirect: '/admin/dashboard', // redirect to the secure profile section
	failureRedirect: '/sign-in' // redirect back to the signup page if there is an error
}), function (req, res) {
	console.log("hello");
	if (req.body.remember) {
		req.session.cookie.maxAge = 900000; //15 minutes
	} else {
		req.session.cookie.expires = false;
	};
	res.redirect('/admin/sign-in');
});



function retrieveDashboardData() {
	var dataToSend = {};

	return Q.Promise(function (resolve, reject, notify) {
		services.dashboard.queryDashboardData()
			.then(function (data) {
				var dashboardData = {
					topicsTotal: data[0].dashboard_data,
					questionsTotal: data[1].dashboard_data,
					quizesTotal: data[2].dashboard_data,
					tablesTotal: data[3].dashboard_data
				};
				dataToSend.dashboardData = dashboardData;
			})
			.then(function () {
				return Q.Promise(function (resolve, reject, notify) {
					services.topics.queryTopicsList().then(function (topicsList) {
						var topics = [];
						for (var i = 0; i < topicsList.length; i++) {
							topics.push({
								id: topicsList[i].id,
								topic: topicsList[i].topic,
								knowledgeOf: topicsList[i].knowledgeOf,
							});
						};
						dataToSend.topicsList = topics;
						resolve("topicsList added to dataToSend object");
					}).fail(function (error) {
						res.status(400).end("Error retrieving topics list");
					});
				});
			})
			.then(function () {
				resolve(dataToSend);
			});
	});
};

router.get('/dashboard', authorization, function (req, res, next) {
	retrieveDashboardData()
		.then(function (dataToSend) {
			return services.dashboard.getTotalWeekUsers()
				.then(function (weekData) {
					return services.dashboard.initTotalWeekUsers(weekData)
						.then(function (weekDashboardData) {
							dataToSend.dashboardData['weekData'] = weekDashboardData;
							return dataToSend;
						});
				});
		})
		.then(function (dataToSend) {
			res.status(200).render('./admin/dashboard', dataToSend);
		})
		.fail(function (error) {
			res.status(400).end("Error retrieving dashboard data");
		});
});

router.get('/quizes', authorization, function (req, res, next) {
	var dataToSend = {};

	services.quizes.queryQuizes()
		.then(function (quizes) {
			return services.quizes.initQuizes(quizes);
		})
		.then(function (quizes) {
			return Q.promise(function (resolve, reject, notify) {
				for (var i = 0; i < 15; i++) {
					quizes = quizes.concat(quizes);

					if (quizes.length >= 75) {
						resolve(quizes);
					};
				};
				// resolve(quizes);
			});
		})
		.then(function (quizes) {
			dataToSend.quizes = quizes;
			res.status(200).render('./admin/quizes', dataToSend);
		})
		.fail(function (error) {
			res.status(400).end('Error retrieving quizes data');
		});
});

router.post('/quizes', authorization, function (req, res, next) {
	var quizData = {
		id: 0,
		name: req.body.quizName || 'Test',
		password: req.body.quizPassword,
		topicId: 1, // THINK ABOUT THIS! using a topic as temp foreign key is not good
		random: 0,
		time: 0,
	};

	bcrypt.genSalt(10, function (err, salt) {
		bcrypt.hash(quizData.password, salt, function (err, hash) {
			if (err) {
				console.log(err);
			} else {
				services.quizes.queryAddQuiz(quizData.name, hash, quizData.topicId, quizData.random, quizData.time)
					.then(function (quizId) {
						quizData.id = quizId;
					})
					.then(function () {
						res.status(201).send({ quizId: quizData.id });
					})
					.fail(function (error) {
						console.log(error);
						res.status(500).send('Error adding question');
					});
			};
		});
	});
});

router.get('/quizes/:quizId', authorization, function (req, res, next) {
	var quizId = parseInt(req.params.quizId) || 0;
	var quiz = {};

	services.quizes.queryQuizes(quizId)
		.then(function (quizes) {
			return services.quizes.initQuizes(quizes);
		})
		.then(function (quizes) {
			quiz = quizes[0];

			return services.quizes.queryQuizQuestions(quizId)
				.then(function (questions) {
					var separator = '~~';
					var questionsData = [];
					questions.forEach(function (question) {
						questionsData.push({
							id: parseInt(question.id),
							name: question.name,
							type: question.type,
							points: parseInt(question.points) || 0,
							answers: (question.answers) ? question.answers.split(separator) : [],
							correctAnswers: (question.correctAnswers) ? question.correctAnswers.split(separator) : []
						});
					});

					return questionsData;
				})
				.then(function (questions) {
					quiz['questions'] = questions;
					delete quiz.password;
					return quiz;
				})
				.fail(function (error) {
					res.status(500).end('Error retrieving quiz questions');
				});
		})
		.then(function (quiz) {
			return services.topics.queryTopicsList()
				.then(function (topics) {
					return { quiz: quiz, topics: topics }
				});
		})
		.then(function (dataToSend) {
			res.status(200).render('./admin/edit-quiz', dataToSend);
		})
		.fail(function (error) {
			res.status(500).end('Error retrieving quizes');
		});
});

router.post('/quizes/:quizId', authorization, function (req, res, next) {
	var quizId = parseInt(req.params.quizId);
	var quizData = JSON.parse(req.body.quizData);

	quizData['id'] = quizId;

	services.quizes.queryUpdateQuiz(quizData)
		.then(function () {
			res.status(200).send('Quiz updated successfully');
		})
		.fail(function (error) {
			console.log(error);
			res.status(500).send('Error updating quiz data');
		});
});

router.delete('/quizes/:quizId', authorization, function (req, res, next) {
	var quizId = parseInt(req.params.quizId);

	services.quizes.queryRemoveQuizes([quizId])
		.then(function () {
			res.status(200).send('Quiz deleted successfully');
		})
		.fail(function (error) {
			console.log(error);
			res.status(500).send('Error deleting quiz');
		});
});

router.delete('/quizes', authorization, function (req, res, next) {
	var quizes = JSON.parse(req.body.quizes);

	services.quizes.queryRemoveQuizes(quizes)
		.then(function () {
			res.status(200).send('Quizes deleted successfully');
		})
		.fail(function (error) {
			console.log(error);
		});
});

router.get('/quizes/:quizId/questions/:questionId', authorization, function (req, res, next) {
	var questionId = parseInt(req.params.questionId);
	var quizId = parseInt(req.params.quizId);
	var types = [];

	if (!quizId || !questionId) {
		res.status(400).render('error');
	};

	services.quizes.queryQuestionTypes()
		.then(function (questionTypes) {
			questionTypes.forEach(function (questionType) {
				types.push({
					id: parseInt(questionType.id),
					name: questionType.name
				});
			});
		})
		.then(function () {
			return services.quizes.queryQuestion(questionId)
		})
		.then(function (question) {
			if (question.answers && question.correctAnswers) {
				question.answers = question.answers.split('~~');
				question.correctAnswers = question.correctAnswers.split('~~');
			};

			question['quizId'] = quizId;
			question['id'] = questionId;

			return question;
		})
		.then(function (question) {
			res.status(200).render('./admin/edit-question', { question: question, types: types });
		}).fail(function (error) {
			res.status(500).end('Error retrieving question types data');
		});
});


router.delete('/quizes/:quizId/questions/:questionId', authorization, function (req, res, next) {
	var quizId = parseInt(req.params.quizId);
	var questionId = parseInt(req.params.questionId);

	if (!quizId || !questionId) {
		res.status(400).send('Invalid quizID or questionID');
	};

	services.quizes.queryRemoveQuestion(questionId)
		.then(function (isSuccess) {
			res.status(200).send('Successfuly deleted question');
		})
		.fail(function (error) {
			res.status(500).send('Error deleting question');
		});
});

router.post('/quizes/:quizId/questions/:questionId', authorization, function (req, res, next) {
	var quizId = parseInt(req.params.quizId);
	var questionId = parseInt(req.params.questionId);
	var questionData = JSON.parse(req.body.questionData);
	var addAnswersPromises = {};

	if (!quizId || !questionId) {
		res.status(400).send('Invalid quizID or questionID');
	};

	services.quizes.queryRemoveAnswers(questionId)
		.then(function () {
			addAnswersPromises = questionData.answers.map(function (answer) {
				services.quizes.queryAddAnswer(answer.name)
					.then(function (answerId) {
						services.quizes.queryAddAnswerToQuestion(questionId, answerId, answer.isCorrect)
							.fail(function (error) {
								console.log(error);
							});
					})
					.fail(function (error) {
						console.log(error);
					});
			})

			Q.all(addAnswersPromises)
				.then(function () {
					services.quizes.queryUpdateQuestion({
						id: questionId,
						name: questionData.name,
						typeId: questionData.typeId,
						points: questionData.points
					}).fail(function (error) {
						console.log(error);
					});
				})
				.then(function () {
					res.status(200).send('Successfully updated question data');
				})
				.fail(function (error) {
					console.log(error);
				});
		})
		.fail(function (error) {
			console.log(error);
		});
});

router.post('/quizes/:quizId/questions', authorization, function (req, res, next) {
	var quizId = parseInt(req.params.quizId) || 0;
	var questionData = {
		name: req.body.questionName || 'Intrebare',
		type: 1,
		points: 1,
		id: 0,
	};

	services.quizes.queryAddQuestion(questionData.name, parseInt(questionData.type), parseInt(questionData.points))
		.then(function (questionId) {
			questionData.id = questionId;
			return questionId;
		})
		.then(function (questionId) {
			return services.quizes.queryAddQuestionToQuiz(quizId, questionId);
		})
		.then(function () {
			res.status(201).send({ questionId: questionData.id });
		})
		.fail(function (error) {
			console.log(error);
			res.status(500).send('Error adding question');
		});
});
//sign-in
router.get('/sign-in', function (req, res) {
	res.render('./admin/sign-in');
});
//logout
router.get('/logout', function (req, res) {
	// user: req.user;
	req.logout();
	res.redirect('/');
});

module.exports = router;