var express = require('express');
var router = express.Router();
var Q = require('q');
var connection = require('../configs/connection');
var services = require('../configs/services');

router.post('/users', function (req, res, next) {
    var userQuizStats = {
        userName: req.body.userName,
        quizId: parseInt(req.body.quizId),
        isGuest: req.body.isGuest,
        mark: parseInt(req.body.mark),
        score: parseInt(req.body.score),
        correct: parseInt(req.body.correct),
        incorrect: parseInt(req.body.incorrect),
        skipped: parseInt(req.body.skipped),
    };

    services.users.queryAddUser(userQuizStats.userName)
        .then(function (userId) {
            services.users.queryAddUserStats(userQuizStats, userId)
                .then(function (isSuccessful) {
                    if (isSuccessful) {
                        // user stats added after quiz completion
                    };
                })
                .fail(function (error) {
                    res.status(500).send('Error adding user stats: ', error);
                });
        })
        .fail(function (error) {
            res.status(500).send('Error adding user: ', error);
        });

    res.json({ userName: userQuizStats.userName });
});

module.exports = router;