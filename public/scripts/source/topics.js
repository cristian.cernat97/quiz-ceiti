$(document).ready(function () {
    'use strict';

    topics = JSON.parse(topics);
    console.log(topics);

    var configElems = {
        $search: '#search',
        $searchInputClear: '.search-input-clear',
        $searchNoResults: '.search-no-results'
    };

    // hide page then display when loaded or after timeout 
    $('header, main, footer').css('display', 'none');
    setTimeout(function () {
        $('#loader-wrapper').fadeOut(function () {
            $(this).remove();
            $('header, main, footer').fadeIn(200);
        });
    }, 200);

    // compile html inside handlebars template and append to DOM
    var templateSource = $('#topics-template').html();
    var template = Handlebars.compile(templateSource);
    $('#quiz-topics-row').append(template(topics));

    // selected topic data
    var topicData = {
        id: 0,
        topic: ""
    };

    $('#quiz-topics-row').on('click', '.quiz-topic', function () {
        $('.modal').modal();
        // get selected topic data
        topicData.id = $(this).data('topic-id');
        topicData.topic = $(this).data('topic-name');
        localStorage.setItem('topicData', JSON.stringify(topicData));
        window.location.href = './quiz-list/' + topicData.id;
    });

    $('#search').on('keyup', _.debounce(function () {
        liveSearch();
    }, 200));

    function liveSearch() {
        var $input;
        var filter;
        var $ul;
        var $li;
        var $a;
        var countHidden = 0;

        $input = $('#search');
        filter = $input.val().toUpperCase();
        $ul = $('#quiz-topics-row');
        $li = $($ul).find('.col');

        for (var i = 0; i < $li.length; i++) {
            $a = $($li[i]).find('.quiz-topic').data('topic-name');
            if ($a.toUpperCase().indexOf(filter) > -1) {
                $($li[i]).css('display', '');
            } else {
                $($li[i]).css('display', 'none');
            };
        };

        countHidden = $('#quiz-topics-row .col:hidden').length;

        if (countHidden == $li.length) {
            $(configElems.$searchNoResults).fadeIn('slow');
        } else {
            $(configElems.$searchNoResults).fadeOut(0);
        };

        $($input).parent().find('i.search-clear').on('click', function () {
            $($input).val('');
            $($input).keyup();
        });
    };

    $(configElems.$searchInputClear).on('click', function () {
        $(configElems.$search).val('');
        $(configElems.$searchNoResults).fadeOut(0);
    });
});