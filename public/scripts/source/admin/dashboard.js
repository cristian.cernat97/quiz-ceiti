$(document).ready(function () {
    'use strict';

    // parse dashboard data retrieved from server and stringified with handlebars helpers
    dashboardData = JSON.parse(dashboardData);
    topicsList = JSON.parse(topicsList);

    console.log(dashboardData);

    loadSidenav();
    $('.collapsible').collapsible();
    $('.button-collapse').sideNav();
    drawGraph();

    function drawGraph() {
        var ctx = document.getElementById("lineChart");
        var data = {
            labels: [],
            datasets: [
                {
                    label: "Numărul de evaluați",
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1,
                    data: [],
                }
            ]
        };

        for (var i = 0; i < dashboardData.weekData.length; i++) {
            var dailyData = dashboardData.weekData[i];
            data.labels.push(dailyData.day);
            data.datasets[0].data.push(dailyData.total);
        };

        var myBarChart = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    };

    function loadSidenav() {
        var template = Handlebars.templates['sidenav.tmpl'];
        $('#sidenav-wrapper').html(template({ pageTitle: 'Dashboard' }));
    };
});